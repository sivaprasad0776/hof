const data = require("./data");

/* 1. Find all Web Developers   */

const re = /^Web Developer I{0,3}/;
const webDevelopersData = data.filter((emp)=>{
    return re.test(emp["job"]) ;
});

const webDevelopernames = [];

webDevelopersData.forEach((emp)=>{
    webDevelopernames.push(`${emp.first_name} ${emp.last_name}`)
});

console.log('Q.No:1 Web Developer names:');
console.log(webDevelopernames);
console.log("\n\n");


/* 2. Convert all the salary values into proper numbers instead of strings  */

const  cleanedData = data.map((emp)=>{
    emp.salary = parseFloat(emp.salary.slice(1));
    return emp;
});

console.log("Q.no:2 After cleaning salary data ")
console.log(cleanedData);
console.log("\n\n");

/*3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something) */

const  faactoredSalaryData = data.map((emp)=>{
    emp.corrected_salary = emp.salary*10000;
    return emp;
});


console.log("Q.No:3 Factored salary")
console.log(faactoredSalaryData);
console.log("\n\n");



/* 4. Find the sum of all salaries  */

const totalSalary = data.reduce((acc, emp)=>{
    return acc + emp.salary;
 }, 0);

 console.log("q.No:4 Sum of all salaries");
 console.log(totalSalary.toFixed(2));
 console.log("\n\n");


 /* 5. Find the sum of all salaries based on country using only HOF method */

 let  countries = data.map((emp)=>{
    return emp.location;
 });

 countries = countries.filter((country, index, ar) => ar.indexOf(country)=== index);

 const salariesByCountry = [];

 countries.forEach((country) =>{
    let salaryByCountry = data.filter((emp)=>{
        return emp.location === country
    }).reduce((acc, emp)=>{
        return acc + emp.salary;
    }, 0);

    salariesByCountry.push({
        country : country,
        totalSalary : salaryByCountry.toFixed(2)
    });
 
 });

 console.log("Q.No:5 Salaries by country:");
 console.log(salariesByCountry);
 console.log("\n\n");


 /* 6. Find the average salary of based on country using only HOF method */


 const avgSalariesByCountry = [];

 countries.forEach((country) =>{
    let salaryStats = data.filter((emp)=>{
        return emp.location === country
    }).reduce(({count, sum}, emp)=>{
        return {count : count+1 , sum: sum+ emp.salary};
    }, {count:0, sum:0});

    avgSalariesByCountry.push({
        country : country,
        avgSalary : (salaryStats.sum/salaryStats.count).toFixed(2)
    });
 
 });

 console.log("Q.No:6 Avg Salaries by Country");
 console.log(avgSalariesByCountry);
 console.log("\n\n");


 

 